#include "gtest/gtest.h"
#include "model_test.h"
#include "core/simulation.h"
#include "core/data_mapper.h"
#include "eigen/Eigen/Eigen"
using namespace boost::math;
typedef  Eigen::ArrayXd Sample;


TEST (DirectedMotion, VELOCITY) {
	ImportFile("dir_motion.xml");
	int replicates = 10;
	
	auto model = TestModel(GetFileString("dir_motion.xml"));
	model.setParam("move_dir", "0.01,0,0");
	model.init();
	const double axial_msd = SIM::findGlobalSymbol<double>("expected_msd_mean") -> get(SymbolFocus::global);
	const double axial_msd_stddev = SIM::findGlobalSymbol<double>("expected_msd_stddev") -> get(SymbolFocus::global);
	
	auto generator =  [&]() -> double {
		model.init();
		model.run();
		return SIM::findGlobalSymbol<VDOUBLE>("velocity") -> get(SymbolFocus::global). abs();
	};
	
	
// 	Map velocity orientation distribution
	EXPECT_NORMAL_DISTRIB(axial_msd, axial_msd_stddev * 1.5, generator);

// 	std::vector<double> results;
// 	results.resize(100);
// 	double sum;
// 	for (auto& el : results) {
// 		sum += el =generator();
// 	}
// 	double mean = sum / 100;
// 	sum = 0;
// 	for (auto& el : results) {
// 		sum += (el-mean)*(el-mean);
// 	}
// 	double stddev = sqrt(sum/100);
// 	cout << std::setprecision(6) << "stats : " << mean << " +- " << stddev << endl; 
	
}
