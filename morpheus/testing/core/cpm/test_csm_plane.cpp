#include "model_test.h"
#include "core/simulation.h"
#include "core/celltype.h"
using namespace Eigen;
const double confidence = 0.95;

// Hex Stats
struct Data {double vol_mean; double vol_sigma; double surface_mean; double surface_sigma; };
std::vector<Data> hex_data =
{ { 1393.58, 3.15, 1090.82, 0.71 },
  { 1268.00, 5.31, 519.77, 0.66 },
  { 874.26, 13.72, 99.57, 0.78 },
  { 1.00, 0.01, 1.73, 0.01 },
  { 1260.31, 5.41, 411.12, 0.33 },
  { 1252.07, 6.82, 242.08, 0.37 },
  { 976.33, 13.38, 106.67, 0.65 },
  { 1.00, 0.01, 1.73, 0.01 },
  { 1255.30, 6.58, 293.14, 0.20 },
  { 1244.45, 5.67, 194.34, 0.24 },
  { 1032.30, 10.79, 110.88, 0.44 },
  { 27.11, 29.07, 9.08, 7.17 },
  { 1252.93, 6.98, 244.21, 0.21 },
  { 1235.83, 6.76, 174.49, 0.20 },
  { 1062.14, 9.58, 113.56, 0.31 },
  { 365.78, 16.97, 63.30, 1.43 }
};


std::vector<Data> square_data =
{ { 1337.63, 3.65, 938.15, 0.65 },
  { 1257.80, 4.28, 366.14, 0.73 },
  { 3.12, 10.07, 2.15, 2.54 },
  { 1.01, 0.01, 1.56, 0.01 },
  { 1257.31, 4.88, 365.82, 0.32 },
  { 1245.89, 5.25, 197.18, 0.33 },
  { 574.26, 15.42, 88.54, 1.16 },
  { 1.00, 0.01, 1.56, 0.01 },
  { 1253.95, 5.19, 266.53, 0.24 },
  { 1235.66, 5.23, 168.25, 0.25 },
  { 718.40, 8.64, 99.45, 0.57 },
  { 1.00, 0.01, 1.56, 0.01 },
  { 1251.19, 5.55, 225.39, 0.20 },
  { 1222.82, 6.25, 156.32, 0.20 },
  { 793.91, 7.45, 104.84, 0.43 },
  { 55.68, 1.81, 27.69, 0.45 }
};


TEST (CSMplane, Hexagonal) {
	
	auto file1 = ImportFile("csm_plane_hex.xml");
	auto model = TestModel(file1.getDataAsString());

	const double init_time = 1200;
	const int data_points = 50;
	const double data_interval = 10;

	auto generator = [&]() -> ArrayXd {
		VectorXd yd(data_points);
		try {
			model.init();
			auto celltype = CPM::getCellTypesMap()["Cell"].lock();
			
			auto cell_volume = celltype->getScope()->findSymbol<double>("cell.volume");
			EXPECT_TRUE(cell_volume);
			auto cell_surface = celltype->getScope()->findSymbol<double>("cell.surface");
			EXPECT_TRUE(cell_surface);
			
			model.run(init_time);
			const auto& cells = celltype->getCellIDs();
			ArrayXd data(2*cells.size());
			
			for (int i=0; i< cells.size(); i++) {
				data[2*i] = cell_volume->get(SymbolFocus(cells[i]));
				data[2*i+1] = cell_surface->get(SymbolFocus(cells[i]));
			}
			
			for (int j=1; j<data_points; j++ ) {
				model.run(data_interval);
				for (int i=0; i< cells.size(); i++) {
					data[2*i] += cell_volume -> get(SymbolFocus(cells[i]));
					data[2*i+1] += cell_surface -> get(SymbolFocus(cells[i]));
				}
			}
			data /= data_points;
			return data;
		}
		catch (const string &e) {
			cout << e;
		}
		catch (const MorpheusException& e) {
			cout << e.what();
		}
		return {};
	};
	
	ArrayXd means(hex_data.size()*2);
	ArrayXd stddevs(hex_data.size()*2);
	int i=0;
	for (const auto& data : hex_data) {
		means[i] = data.vol_mean; stddevs[i]=data.vol_sigma;
		i++;
		means[i] = data.surface_mean; stddevs[i] = data.surface_sigma;
		i++;
	}
	
	EXPECT_NORMAL_DISTRIB(means, stddevs, generator);
}

TEST (CSMplane, Square) {
	
	auto file1 = ImportFile("csm_plane_square.xml");
	auto model = TestModel(file1.getDataAsString());

	const double init_time = 1200;
	const int data_points = 50;
	const double data_interval = 10;

	auto generator = [&]() -> ArrayXd {
		VectorXd yd(data_points);
		try {
			model.init();
			auto celltype = CPM::getCellTypesMap()["Cell"].lock();
			
			auto cell_volume = celltype->getScope()->findSymbol<double>("cell.volume");
			EXPECT_TRUE(cell_volume);
			auto cell_surface = celltype->getScope()->findSymbol<double>("cell.surface");
			EXPECT_TRUE(cell_surface);
			
			model.run(init_time);
			const auto& cells = celltype->getCellIDs();
			ArrayXd data(2*cells.size());
			
			for (int i=0; i< cells.size(); i++) {
				data[2*i] = cell_volume->get(SymbolFocus(cells[i]));
				data[2*i+1] = cell_surface->get(SymbolFocus(cells[i]));
			}
			
			for (int j=1; j<data_points; j++ ) {
				model.run(data_interval);
				for (int i=0; i< cells.size(); i++) {
					data[2*i] += cell_volume -> get(SymbolFocus(cells[i]));
					data[2*i+1] += cell_surface -> get(SymbolFocus(cells[i]));
				}
			}
			data /= data_points;
			return data;
		}
		catch (const string &e) {
			cout << e;
		}
		catch (const MorpheusException& e) {
			cout << e.what();
		}
		return {};
	};
	
	ArrayXd means(square_data.size()*2);
	ArrayXd stddevs(square_data.size()*2);
	int i=0;
	for (const auto& data : square_data) {
		means[i] = data.vol_mean; stddevs[i]=data.vol_sigma;
		i++;
		means[i] = data.surface_mean; stddevs[i] = data.surface_sigma;
		i++;
	}
	
	EXPECT_NORMAL_DISTRIB(means, stddevs, generator);
}
