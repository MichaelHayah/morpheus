//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#ifndef FWD_EULER_AD_SOLVER_H
#define FWD_EULER_AD_SOLVER_H

#include "ad_solver.h"


class WellMixedSolver : public AD_Solver_Base
{
public:
	WellMixedSolver(const AD_Descriptor &d) : AD_Solver_Base(d) {
// 		auto field = d.field_symbol->getField();
		std::cout << "WellMixedSolver " ;
		if (d.field_type == AD_Descriptor::FIELD) std::cout << " Field" << std::endl;
		else cout << " Membrane" << std::endl;
	}
	void setNodeLength(double value) override {};
	double maxTimeStepHint() const override { return std::numeric_limits<double>::max(); };
	// compute the mixing
	int computeTimeStep(SymbolFocus focus, double time_step, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1) override;
};

class FwdEulerADSolver : public AD_Solver_Base
{
public:
	FwdEulerADSolver(const AD_Descriptor &d);
	
	double maxTimeStepHint() const override;
	
	int computeTimeStep(SymbolFocus focus, double time_step, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1) override;
	
	void setNodeLength(double value) override { node_length = value; }
// 	void writeSolution(void) const { data.field_symbol->applyBuffer(); };
	
private:
	bool solve_fwd_euler_diffusion_homogeneous(double time_interval, double node_length, AD_Descriptor::scalar_tensor* diffusion,  AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1);
// 	void fixFwdEulerBoundaries(double time_interval, double node_flux, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1);
// 	bool solve_fwd_euler_diffusion_generalized(double time_interval, double node_flux, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1);
	bool solve_fwd_euler_diffusion_spheric(double time_interval, double node_length, AD_Descriptor::scalar_tensor* diffusion,  AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1);
	bool solve_fwd_euler_diffusion_heterogeneous(double time_interval, double node_length,  AD_Descriptor::scalar_tensor *diffusion_rate, AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1);
	bool solve_well_mixed(double time_interval, double node_length, AD_Descriptor::scalar_tensor* diffusion,  AD_Descriptor::scalar_tensor *x0, AD_Descriptor::scalar_tensor *x1);
	
	const Lattice* lattice;
	Neighborhood Neighborhood_1; 
	double node_length;
};

#endif
