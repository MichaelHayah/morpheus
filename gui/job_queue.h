//////
//
// This file is part of the modelling and simulation framework 'Morpheus',
// and is made available under the terms of the BSD 3-clause license (see LICENSE
// file that comes with the distribution or https://opensource.org/licenses/BSD-3-Clause).
//
// Authors:  Joern Starruss and Walter de Back
// Copyright 2009-2016, Technische Universität Dresden, Germany
//
//////

#ifndef JOB_QUEUE_H
#define JOB_QUEUE_H
#include <QThread>
#include <QSharedPointer>
#include <QList>
#include <QMap>
#include "localprocess.h"
#include "remoteprocess.h"

class parameterSweeper;

class JobQueue : public QObject
{
    Q_OBJECT
public:
    static QStringList queueNames();

    JobQueue(QObject* parent = 0);
    ~JobQueue();

    const QMap<int, QSharedPointer<abstractProcess> >& getJobs();
	bool isSweep(int job_id);
	QList<int> getInteractiveJobs() { QList<int> k(running_interactive_jobs); k.append(pending_interactive_jobs); return k; };
	int jobCount() { return maxJobID(); };  /*!< Returns the number of Jobs done*/
	int modelCount(); /*!< Returns the number of Models done*/

public slots:
	/*!< Start the job queue. */
	void run();
    void stop();
    int addProcess(SharedMorphModel model, bool rerun = false);
	void addSweep(SharedMorphModel model, int sweep_id);

	void stopProcess(int id);                     ///  Stops the job @p id.
    void stopProcesses(const QList<int>& ids);
    void removeProcess(int id, abstractProcess::RemovePolicy remove_policy ); /// Removes the job @p id.
    void removeProcesses(const QList<int>& ids, abstractProcess::RemovePolicy remove_policy);
    void debugProcess(int id);                    /// Removes the job @p id.

private:
    QMap<int, QSharedPointer<abstractProcess> > jobs;
    QList<int> pending_jobs;
    QList<int> pending_interactive_jobs;
    QList<int> running_jobs;
	QList<int> running_interactive_jobs;
	QList<int> finished_interactive_jobs;
    bool is_initialized;

    QTimer *timer; /*!< Timer which defines the intervals after those the list of startable jobs is checked. */

    void restoreSavedJobs(); /*!< Restore jobs from the QSettings. */
	void blockQueue(bool block = true);
	bool block_state;
    enum QueueType { local, interactive, remote };
    QMap<QString,QueueType> queueNamesMap();
	int maxJobID();
    int newJobID(); /*!< Returns a free Job ID */
    int lastJobForTitle(QString title);

private slots:
    void processStateChanged(abstractProcess *process);
    /*!< Update the informations to the given process, when its state changed. */
    void processQueue(); /*!< Checks the queue for jobs that can be started. */
	void processError(QString error);

signals:
	void statusMessage(QString message, int progress=-1);
	void criticalMessage(QString message, int job_id=-1);
    void processAdded(int);
	void processesAdded(QList<int>);
    void processChanged(int);
    void processRemoved(int);
};

#endif // JOB_QUEUE_H
