#pragma once
#include <vector>
#include <map>
#include <string>
#include <functional>

#ifdef ERROR
#undef ERROR
#endif
#ifdef DELETE
#undef DELETE
#endif

namespace MorpheusML {
	struct AutoFix {
		std::string match_path;
		std::string target_path;
		std::string require_path;
		std::string message;
		enum Operation { COPY, MOVE, DELETE } operation;
		bool replace_existing;
		std::map<std::string,std::string> value_conversions;
		enum Severty { SILENT, INFO, WARN, CHECK, ERROR } severty;
		AutoFix() : operation(MOVE), replace_existing(true), severty(Severty::INFO) {};
	};
	
	std::map<std::pair<int,int>, std::vector<AutoFix> > getRuleSets();
}
